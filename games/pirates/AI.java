/**
 * This is where you build your AI for the Pirates game.
 */
package games.pirates;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import joueur.BaseAI;

// <<-- Creer-Merge: imports -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
// you can add additional import(s) here
// <<-- /Creer-Merge: imports -->>

/**
 * This is where you build your AI for the Pirates game.
 */
public class AI extends BaseAI {
    /**
     * This is the Game object itself, it contains all the information about the current game
     */
    public Game game;

    /**
     * This is your AI's player. This AI class is not a player, but it should command this Player.
     */
    public Player player;

    // <<-- Creer-Merge: fields -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
    // you can add additional fields here for your AI to use
    // <<-- /Creer-Merge: fields -->>


    /**
     * This returns your AI's name to the game server. Just replace the string.
     * @return string of you AI's name
     */
    public String getName() {
        // <<-- Creer-Merge: get-name -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        return "Pirates Java Player"; // REPLACE THIS WITH YOUR TEAM NAME!
        // <<-- /Creer-Merge: get-name -->>
    }

    /**
     * This is automatically called when the game first starts, once the Game object and all GameObjects have been initialized, but before any players do anything.
     * This is a good place to initialize any variables you add to your AI, or start tracking game objects.
     */
    public void start() {
        // <<-- Creer-Merge: start -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        super.start();
        // <<-- /Creer-Merge: start -->>
    }

    /**
     * This is automatically called every time the game (or anything in it) updates.
     * If a function you call triggers an update this will be called before that function returns.
     */
    public void gameUpdated() {
        // <<-- Creer-Merge: game-updated -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        super.gameUpdated();
        // <<-- /Creer-Merge: game-updated -->>
    }

    /**
     * This is automatically called when the game ends.
     * You can do any cleanup of you AI here, or do custom logging. After this function returns the application will close.
     * @param  won  true if your player won, false otherwise
     * @param  name  reason">a string explaining why you won or lost
     */
    public void ended(boolean won, String reason) {
        // <<-- Creer-Merge: ended -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        super.ended(won, reason);
        // <<-- /Creer-Merge: ended -->>
    }


    /**
     * This is called every time it is this AI.player's turn.
     *
     * @return Represents if you want to end your turn. True means end your turn, False means to keep your turn going and re-call this function.
     */
    public boolean runTurn() {
        // <<-- Creer-Merge: runTurn -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        // Put your game logic here for runTurn
    	
    	//Notes:
    	// Unfortunatly in the java client if a function you call is invalidated 
    	// (aka you deposit more than you have or try to move a ship through land)
    	// You will get a nullpointer exception. In these cases just pay attention to the
    	// Error that was printed before the exception.
    	//
    	// Also, since this will come up as a question, when your units merge (while moving),
        // the unit you're merging into will "die". This means its tile will be set to null.
        // Make sure to check that your unit's tile is not null before trying to control it!
    	if(this.player.units.isEmpty())
    	{
    		//Spawn crew if we have no units
    		this.player.port.spawn("crew");
    	}
    	else if(this.player.units.get(0).shipHealth==0)
    	{
    		//Spawn a ship so our crew can sail
    		this.player.port.spawn("ship");
    	}
    	else if (this.player.units.get(0).shipHealth < (this.game.shipHealth / 2))
        {
            // Heal our unit if the ship is almost dead
            // Note: Crew also have their own health. Maybe try adding a check to see if the crew need healing?
            Unit unit = this.player.units.get(0);
            // Find a path to our port so we can heal
            List<Tile> path = this.findPath(unit.tile, this.player.port.tile, unit);
            if (path.size() > 0) {
                // Move along the path if there is one
                unit.move(path.get(0));
            }
            else 
            {
            	// Try to deposit any gold we have while we're here
            	if(unit.gold>0&&unit.tile.port!=null)
            		unit.deposit(unit.gold);
                // Try to rest
                unit.rest();
            }
        }
        else
        {
            // Try to attack a merchant
            Unit unit = this.player.units.get(0);
            // Look for a merchant ship
            Unit merchant = null;
            for(Unit u : this.game.units)
            {
                if (u.targetPort != null)
                {
                    // Found one
                    merchant = u;
                    break;
                }
            }

            // If we found a merchant, move to it, then attack it
            if (merchant != null) 
            {
                // Find a path to this merchant
                List<Tile> path = this.findPath(unit.tile, merchant.tile, unit);
                if (path.size() > this.game.shipRange) 
                {
                    // Move until we're within firing range of the merchant
                    // Note: Range is *circular* in pirates, so this can be improved on
                    unit.move(path.get(0));
                }
                else
                {	
                    // Try to attack the merchant's ship
                    unit.attack(merchant.tile, "ship");
                }
            }
        }
        return true;
        // <<-- /Creer-Merge: runTurn -->>
    }


    /**
     * A very basic path finding algorithm (Breadth First Search) that when given a starting Tile, will return a valid path to the goal Tile.
     * @param  start  the starting Tile
     * @param  goal  the goal Tile
     * @param  unit  the Unit that will move
     * @return A List of Tiles representing the path, the the first element being a valid adjacent Tile to the start, and the last element being the goal. Or an empty list if no path found.
     */
    List<Tile> findPath(Tile start, Tile goal, Unit unit) {
        // no need to make a path to here...
        if (start == goal) {
            return new ArrayList<Tile>();
        }

        // the tiles that will have their neighbors searched for 'goal'
        Queue<Tile> fringe = new LinkedList<Tile>();

        // How we got to each tile that went into the fringe.
        HashMap<Tile, Tile> cameFrom = new HashMap<Tile, Tile>();

        // Enqueue start as the first tile to have its neighbors searched.
        fringe.add(start);

        // keep exploring neighbors of neighbors... until there are no more.
        while (!fringe.isEmpty()) {
            // the tile we are currently exploring.
            Tile inspect = fringe.remove();
            
            // cycle through the tile's neighbors.
            List<Tile> neighbors = inspect.getNeighbors();
            for (int i = 0; i < neighbors.size(); i++) {
                Tile neighbor = neighbors.get(i);

                // If we found the goal we've found the path!
                if (neighbor == goal) {
                    // Follow the path backward starting at the goal and return it.
                    List<Tile> path = new ArrayList<Tile>();
                    path.add(goal);

                    // Starting at the tile we are currently at, insert them retracing our steps till we get to the starting tile
                    for (Tile step = inspect; step != start; step = cameFrom.get(step)) {
                        path.add(0, step);
                    }

                    return path;
                }
                // if the tile exists, has not been explored or added to the fringe yet, and it is pathable
                if (neighbor != null && !cameFrom.containsKey(neighbor) && neighbor.isPathable(unit)) { 
                    // add it to the tiles to be explored and add where it came from.
                    fringe.add(neighbor);
                    cameFrom.put(neighbor, inspect);
                }

            } // for each neighbor

        } // while fringe not empty

        // if you're here, that means that there was not a path to get to where you want to go.
        //   in that case, we'll just return an empty path.
        return new ArrayList<Tile>();
    }

    // <<-- Creer-Merge: methods -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
    // you can add additional methods here for your AI to call
    // <<-- /Creer-Merge: methods -->>
}
